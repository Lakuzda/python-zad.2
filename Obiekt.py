class A:
    def __init__(self, number):
        self.number = number
        print("Your number is {}".format(self.number))



class B(A):
    def __init__(self, number):
        super(B, self).__init__(number)
        self.number += 1
        print("{} is greater".format(self.number))


number_one = B(1)
