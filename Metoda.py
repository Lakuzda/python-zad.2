class A:
    def __init__(self, number):
        self.number = number
        print("Creating number")

    def your_number_is(self):
        print("Your number is {}".format(self.number))


class B(A):
    def __init__(self, number):
        super(B, self).__init__(number)

    def your_number_greater(self):
        self.number = self.number * 2
        print("Your number is now two times greater. Your number is {}".format(self.number))


num = B(5)

num.your_number_is()
num.your_number_greater()
